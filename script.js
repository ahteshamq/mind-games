const gameContainer = document.getElementById("game");
const btnReset = document.getElementById("reset")
const btnPlayAgain = document.getElementById("again")
const btnInstructions = document.getElementById("howTo")
const btnClose = document.getElementById("close")
const btnStart = document.getElementById("start")
const btnEasy = document.getElementById("easy")
const btnMedium = document.getElementById("medium")
const btnHard = document.getElementById("hard")

const instruction = document.querySelector('.how-to')
const yourScore = document.querySelector('.your-score')
const highScore = document.querySelector('.high-score')
const board = document.querySelector('.board')
const level = document.querySelector('.level')

const COLORS = [
  "red",
  "red",
  "blue",
  "blue",
  "green",
  "green",
  "orange",
  "orange",
  "purple",
  "purple",
  "six",
  "six",
  "seven",
  "seven",
  "eight",
  "eight",
  "nine",
  "nine",
  "ten",
  "ten",
  "eleven",
  "eleven",
  "twelve",
  "twelve"
];
// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let levels;
let score = 0

// let game;
btnEasy.addEventListener("click", shuffle1);
function shuffle1() {
  levels = 12;
  let game = 6;
  let shuffledColors = shuffle(COLORS.slice(0, levels));
  level.style.display = "none";
  board.style.display = "flex";
  createDivsForColors(shuffledColors, game);
  btnReset.style.display = "block";
  let preScore = localStorage.getItem('level6');
  if (preScore === null) {
    highScore.textContent = 'High score: --'
  } else {
    highScore.textContent = `High score: ${preScore}`
  }
}

btnMedium.addEventListener("click", shuffle2);
function shuffle2() {
  levels = 18;
  let game = 9;
  let shuffledColors = shuffle(COLORS.slice(0, levels));
  level.style.display = "none";
  board.style.display = "flex";
  createDivsForColors(shuffledColors, game);
  btnReset.style.display = "block";
  let preScore = localStorage.getItem('level9');
  if (preScore === null) {
    highScore.textContent = 'High score: --';
  } else {
    highScore.textContent = `High score: ${preScore}`;
  }
}

btnHard.addEventListener("click", shuffle3);
function shuffle3() {
  let game = 12;
  let shuffledColors = shuffle(COLORS);
  level.style.display = "none";
  board.style.display = "flex";
  createDivsForColors(shuffledColors, game);
  btnReset.style.display = "block";
  let preScore = localStorage.getItem('level12');
  if (preScore === null) {
    highScore.textContent = 'High score: --';
  } else {
    highScore.textContent = `High score: ${preScore}`
  }
}








// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray, game) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");
    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);
    newDiv.classList.add('color-hidden');
    newDiv.myParam = game
    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

let clickedCard = null;
let preventClick = null;
let match = 0






// TODO: Implement this function!
function handleCardClick(event) {
  let won = event.currentTarget.myParam
  if (preventClick) {
    return
  }

  // current target
  curTarget = event.target

  if (curTarget === clickedCard || curTarget.className.includes('freeze')) {
    return;
  }
  // you can use event.target to see which element was clicked
  score += 1
  yourScore.textContent = `Your score: ${score}`

  curTarget.classList.remove('color-hidden')

  curTarget.classList.add('freeze')

  if (!clickedCard) {
    clickedCard = curTarget
  } else if (clickedCard) {
    if (clickedCard.className !== curTarget.className) {
      preventClick = true
      setTimeout(() => {
        clickedCard.classList.remove('freeze');
        curTarget.classList.remove('freeze');
        curTarget.classList.add('color-hidden');
        clickedCard.classList.add('color-hidden');
        preventClick = false;
        clickedCard = null;
      }, 1000);
    } else {
      clickedCard = null;
      match += 1;
      if (match === won) {
        const victory = document.querySelector('.victory');
        victory.style.display = 'block';
        gameContainer.style.display = 'none';
        btnReset.style.display = 'none'
        btnPlayAgain.style.display = 'block';
        updateScore(score, won);
        score = 0
      }

    }
  }


}





btnStart.addEventListener('click', function () {
  btnStart.style.display = 'none'
  level.style.display = 'flex'
})

btnReset.addEventListener('click', reloadPage)
btnPlayAgain.addEventListener('click', reloadPage)

function reloadPage() {
  window.location.reload()
}

btnInstructions.addEventListener('click', function () {
  instruction.style.display = 'flex'
  gameContainer.style.display = 'none'
})

btnClose.addEventListener('click', function () {
  instruction.style.display = 'none'
  gameContainer.style.display = 'flex'
})


// when the DOM loads

yourScore.textContent = `Your score: ${score}`


function updateScore(score, won) {
  let key = `level${won}`
  let preScore = localStorage.getItem(key);
  if (preScore === null) {
    highScore.textContent = `High score: ${score}`
    localStorage.setItem(key, score)
  } else {
    if (score < parseInt(preScore)) {
      localStorage.setItem(key, score)
      highScore.textContent = `High score: ${score}`
    }
  }

}